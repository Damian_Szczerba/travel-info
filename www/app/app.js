require(['bootstrap']);

define(['angularAMD', 'angular', 'angular-ui-router', 'ui-bootstrap'], function (angularAMD) {
    var app = angular.module("app", ['ui.router', 'ui.bootstrap']);
    app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $stateProvider


        .state('root.directions', angularAMD.route({
            url: '/directions',
            views: {
                '@': angularAMD.route({
                    templateUrl: 'app/views/directions.html',
                    controllerUrl: 'controllers/DirectionsController'
                })
            }
        }));

        $urlRouterProvider
            .otherwise("/directions");
        }]);

    return angularAMD.bootstrap(app);
});