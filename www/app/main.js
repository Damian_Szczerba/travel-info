var localPaths = {
    "jquery": '../assets/js/jquery/dist/jquery',
    "angular": '../assets/js/bower_components/angular/angular',
    "angularAMD": '../assets/js/angularAMD/angularAMD',
    "ngload": '../assets/js/angularAMD/ngload',
    "angular-ui-router": '../assets/js/angular-ui-router/release/angular-ui-router',
    "bootstrap": '../assets/js/bootstrap/dist/js/bootstrap.min',
    "fast-click": '../assets/plugins/fast-click/fastclick',
    "ui-bootstrap": '../assets/js/ui-bootstrap/ui-bootstrap.min'
};

var apiPath = 'http://localhost:8080';

var paths = localPaths;

require.config({
    baseUrl: 'app',
    paths: paths,
    shim: {
        'angular': ['jquery'],
        'angularAMD': ['angular'],
        'angular-route': ['angular'],
        'angular-resource': ['angular'],
        'angular-ui-router': ['angular'],
        'bootstrap': ['jquery'],
        'ngload': ['angularAMD'],
        'ui-bootstrap': ['angular']
    },

    deps: ['app']
});